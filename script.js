db.users.insertMany([
	{
		"firstName":"Diane",
		"lastName": "Murphy",
		"email" : "dmurphy@mail.com",
		"isAdmin": "no",
		"isActive":"yes"
	},
	{
		"firstName":"Mary",
		"lastName": "Patterson",
		"email" : "mpatterson@mail.com",
		"isAdmin": "no",
		"isActive":"yes"
	},
	{
		"firstName":"Jeff",
		"lastName": "Firrelli",
		"email" : "jfirrelli@mail.com",
		"isAdmin": "no",
		"isActive":"yes"
	},
	{
		"firstName":"Gerard",
		"lastName": "Bondur",
		"email" : "gbondur@mail.com",
		"isAdmin": "no",
		"isActive":"yes"
	},
	{
		"firstName":"Pamela",
		"lastName": "Castillo",
		"email" : "pcastillo@mail.com",
		"isAdmin": "yes",
		"isActive":"no"
	},
	{
		"firstName":"George",
		"lastName": "Vanauf",
		"email" : "gvanauf@mail.com",
		"isAdmin": "yes",
		"isActive":"yes"
	}
]);

//add following courses
db.courses.insertMany(
	{
		"name": "Professional Development",
		"price": "10,000.00"
	},
	{
		"name": "Business Processing",
		"price": "13,000.00"
	}
);


//get user ids and add them as enrollees of the course(update)
db.courses.updateOne(
		{
			"name": "Professional Development"
		},
		{
			$set: {
				"Enrollees": [
					{
						"userID" :ObjectId("6125b67648d587748c727346")
					},
					{
						"userID" :ObjectId("6125b67648d587748c727348")
					}
				]
			}
		}
	);

db.courses.updateOne(
		{
			"name": "Business Processing"
		},
		{
			$set:
			{
				"Enrollees": [
					{
						"userID" :ObjectId("6125b67648d587748c727349")
					},
					{
						"userID" :ObjectId("6125b67648d587748c727347")
					}
				]
			}
		}
	);

//get users who are not administrators
db.users.find(
	{
		"isAdmin": "no"
	}
);